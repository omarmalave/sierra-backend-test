import { Test, TestingModule } from '@nestjs/testing';
import { ListingsController } from './listings.controller';
import { ListingsService } from './listings.service';

describe('ListingsController', () => {
  let controller: ListingsController;
  const listinServiceMock = {
    create: jest.fn(),
    findAll: jest.fn(),
    remove: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ListingsController],
      providers: [
        {
          provide: ListingsService,
          useValue: listinServiceMock,
        },
      ],
    }).compile();

    controller = module.get<ListingsController>(ListingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should call listingsService.create and return the created listing', () => {
      const createListingDto = {
        title: 'Test',
        description: 'Test',
        price: 100,
      };

      listinServiceMock.create.mockReturnValueOnce({
        id: '1',
        ...createListingDto,
      });

      const result = controller.create(createListingDto);

      expect(listinServiceMock.create).toHaveBeenCalledWith(createListingDto);
      expect(result).toEqual({
        id: '1',
        title: 'Test',
        description: 'Test',
        price: 100,
      });
    });
  });

  describe('findAll', () => {
    it('should call listingsService.findAll and return all listings', () => {
      listinServiceMock.findAll.mockReturnValueOnce([
        {
          id: '1',
          title: 'Test',
          description: 'Test',
          price: 100,
        },
      ]);

      const result = controller.findAll();

      expect(listinServiceMock.findAll).toHaveBeenCalled();
      expect(result).toEqual([
        {
          id: '1',
          title: 'Test',
          description: 'Test',
          price: 100,
        },
      ]);
    });

    it('should call listingsService.findAll and return an empty array', () => {
      listinServiceMock.findAll.mockReturnValueOnce([]);

      const result = controller.findAll();

      expect(listinServiceMock.findAll).toHaveBeenCalled();
      expect(result).toEqual([]);
    });
  });

  describe('remove', () => {
    it('should call listingsService.remove and return succes response', () => {
      listinServiceMock.remove.mockReturnValueOnce({ success: true });

      const result = controller.remove('1');

      expect(listinServiceMock.remove).toHaveBeenCalledWith('1');
      expect(result).toEqual({ success: true });
    });
  });
});
