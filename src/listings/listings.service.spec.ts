import { Test, TestingModule } from '@nestjs/testing';
import { ListingsService } from './listings.service';

describe('ListingsService', () => {
  let service: ListingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ListingsService],
    }).compile();

    service = module.get<ListingsService>(ListingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a new listing', () => {
      const createListingDto = {
        title: 'Test',
        description: 'Test',
        price: 100,
      };

      const result = service.create(createListingDto);

      expect(result).toEqual({
        id: expect.any(String),
        ...createListingDto,
      });
    });

    it('should add the new listing to the list of listings', () => {
      const createListingDto = {
        title: 'Test',
        description: 'Test',
        price: 100,
      };

      const result = service.create(createListingDto);

      expect(service.findAll()).toContain(result);
    });

    it('should throw an error if the the input is null/undefined', () => {
      expect(() => service.create(null)).toThrow();
      expect(() => service.create(undefined)).toThrow();
    });
  });

  describe('findAll', () => {
    it('should return all listings', () => {
      const createListingDto = {
        title: 'Test',
        description: 'Test',
        price: 100,
      };
      const listing = service.create(createListingDto);

      const listings = service.findAll();

      expect(listings).toContain(listing);
    });

    it('should return an empty array if there are no listings', () => {
      const listings = service.findAll();

      expect(listings).toEqual([]);
    });
  });

  describe('remove', () => {
    it('should remove a listing', () => {
      const createListingDto = {
        title: 'Test',
        description: 'Test',
        price: 100,
      };
      const listing = service.create(createListingDto);

      const result = service.remove(listing.id);

      expect(result).toEqual({ success: true });
      expect(service.findAll()).not.toContain(listing);
    });

    it('should throw an error if the listing does not exist', () => {
      expect(() => service.remove('1')).toThrow();
    });
  });
});
