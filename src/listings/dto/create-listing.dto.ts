import { IsCurrency, IsString } from 'class-validator';

export class CreateListingDto {
  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsCurrency()
  price: number;
}
