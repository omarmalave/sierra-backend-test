import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateListingDto } from './dto/create-listing.dto';
import { Listing } from './entities/listing.entity';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ListingsService {
  private listings: Listing[] = [];

  create(createListingDto: CreateListingDto) {
    const { title, description, price } = createListingDto;

    const newListing: Listing = {
      id: uuidv4(),
      title,
      description,
      price,
    };

    this.listings.push(newListing);

    return newListing;
  }

  findAll() {
    return this.listings;
  }

  remove(id: string) {
    const index = this.listings.findIndex((l) => l.id === id);

    if (index < 0) {
      throw new NotFoundException(`Listing with id ${id} not found`);
    }

    this.listings.splice(index, 1);

    return { success: true };
  }
}
