# Sierra Backend Test

## Description

Code for Sierra Backend live screening [test](https://docs.google.com/document/d/1orGsvyWqcY9o9g-Wer8ttEI8XDnRnbfxQkMVoDt1S-g/edit).

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```