import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

// mock uuidv4
jest.mock('uuid', () => {
  return {
    v4: () => '1',
  };
});
describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('GET /listings', () => {
    it('should return an empty array of listings', () => {
      return request(app.getHttpServer())
        .get('/listings')
        .expect(200)
        .expect([]);
    });

    it('should return all listings', () => {
      return request(app.getHttpServer())
        .post('/listings')
        .send({ title: 'Test', description: 'Test', price: '100.00' })
        .expect(201)
        .then(() => {
          return request(app.getHttpServer())
            .get('/listings')
            .expect(200)
            .expect([
              {
                id: '1',
                title: 'Test',
                description: 'Test',
                price: '100.00',
              },
            ]);
        });
    });
  });

  describe('POST /listings', () => {
    it('should create a new listing', () => {
      return request(app.getHttpServer())
        .post('/listings')
        .send({ title: 'Test', description: 'Test', price: '100.00' })
        .expect(201)
        .expect({
          id: '1',
          title: 'Test',
          description: 'Test',
          price: '100.00',
        });
    });

    it('should add the new listing to the list of listings', () => {
      return request(app.getHttpServer())
        .post('/listings')
        .send({ title: 'Test', description: 'Test', price: '100.00' })
        .expect(201)
        .then(() => {
          return request(app.getHttpServer())
            .get('/listings')
            .expect(200)
            .expect([
              {
                id: '1',
                title: 'Test',
                description: 'Test',
                price: '100.00',
              },
            ]);
        });
    });

    it('should throw an error if the input is invalid', () => {
      return request(app.getHttpServer())
        .post('/listings')
        .send({ title: 'Test', description: 'Test' })
        .expect(400);
    });
  });

  describe('DELETE /listings/:id', () => {
    it('should remove a listing', () => {
      return request(app.getHttpServer())
        .post('/listings')
        .send({ title: 'Test', description: 'Test', price: '100.00' })
        .expect(201)
        .then((response) => {
          return request(app.getHttpServer())
            .delete(`/listings/${response.body.id}`)
            .expect(200)
            .expect({ success: true });
        });
    });

    it('should throw an error if the listing does not exist', () => {
      return request(app.getHttpServer()).delete('/listings/1').expect(404);
    });
  });
});
